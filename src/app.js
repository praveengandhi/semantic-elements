export class App {
  constructor() {
    this.message = 'Semantic Aurelia!';
    this.buttonColor = 'pink';
    this.colors = ['red','orange','yellow','olive','green','teal','blue','violet','purple','pink','brown','grey','black'];
  }
}


export class CapitalValueConverter {
  toView(value) {
    return value && value.charAt(0).toUpperCase() + value.slice(1);
  }
}