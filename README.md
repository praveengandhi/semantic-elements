# Simple steps to add semantic ui components from https://github.com/ged/aurelia-semantic-ui to your project

1. au new
2. add lib directory (jQuery and Semantic-ui)
3. get src from https://github.com/ged/aurelia-semantic-ui and paste in project's src
4. comment import jquery and semantic-ui in index.js as they are being loaded in index.html
5. add new feature (semantic) in main.js
6. add some semantic elements in app module to test


# while deploying output of aurelia build (au build --env prod)
1. au build --env prod
2. copy jquery.js and sematic.min.js code to vendor-bundle.js
3. remove jquery and semanti.min.js reference in index.html
4. deployment package should include index.html, scripts and lib (can delete jquery.js and semantic.min.js as they are included in vendor-bundle.js)